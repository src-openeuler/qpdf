Name:           qpdf
Version:        11.1.0
Release:        3
Summary:        A command-line program to transform PDF files
License:        (Artistic 2.0 or ASL 2.0) and MIT
URL:            http://qpdf.sourceforge.net/
Source0:        http://downloads.sourceforge.net/sourceforge/qpdf/qpdf-%{version}.tar.gz

Patch0001:      0001-Tune-QUtil-hex_encode.patch

BuildRequires:  gcc gcc-c++ zlib-devel libjpeg-turbo-devel pcre-devel
BuildRequires:  perl-interpreter perl-generators perl(Digest::MD5)
BuildRequires:  cmake gnutls-devel

Provides:       %{name}-libs = %{version}-%{release}
Obsoletes:      %{name}-libs < %{version}-%{release}

%description
QPDF is a command-line program that does structural, content-preserving transformations
on PDF files. It could have been called something like pdf-to-pdf. It also provides many
useful capabilities to developers of PDF-producing software or for people who just want
to look at the innards of a PDF file to learn more about how they work.


%package devel
Summary:        Development package for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains some header and library files for the development of %{name}.

%package help
Summary:        Help package for %{name}
BuildArch:      noarch
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description help
This package contains some man help and other files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
export LDFLAGS="${RPM_LD_FLAGS}"

cmake -S . -B build \
    -DCMAKE_C_FLAGS_RELEASE:STRING=-DNDEBUG \
    -DCMAKE_CXX_FLAGS_RELEASE:STRING=-DNDEBUG \
    -DCMAKE_Fortran_FLAGS_RELEASE:STRING=-DNDEBUG \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_INSTALL_DO_STRIP:BOOL=OFF \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DBUILD_STATIC_LIBS=0 \
    -DREQUIRE_CRYPTO_GNUTLS=1 \
    -DUSE_IMPLICIT_CRYPTO=0 \
    -DSHOW_FAILED_TEST_OUTPUT=1 \
    -DINSTALL_CMAKE_PACKAGE=0
cmake --build build

%install
cmake --install build --prefix %{buildroot}/usr
rm -rf %{buildroot}/usr/lib64/cmake
rm -rf %{buildroot}/usr/lib64/libqpdf.a

%delete_la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc README.md TODO ChangeLog
%license Artistic-2.0
%{_bindir}/*
%{_libdir}/libqpdf.so.*

%files devel
%doc examples/*.cc examples/*.c
%{_includedir}/qpdf/
%{_libdir}/libqpdf.so
%{_libdir}/pkgconfig/libqpdf.pc

%files help
%{_pkgdocdir}
%{_mandir}/man1/*

%changelog
* Fri May 10 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 11.1.0-3
- Tune QUtil::hex_encode

* Mon Mar 06 2023 wangkai <wangkai385@h-partners.com> - 11.1.0-2
- Add BIND_NOW,PIE,SP secure compilation options

* Wed Oct 12 2022 humengfei <humengfei@tju.edu.cn> - 11.1.0-1
- update

* Sun Dec 15 2019 caodongxia <caodongxia@huawei.com> - 8.4.2-3
- fix rpmbuild missing patches due to different arch

* Mon Dec 16 2019 catastrowings <jianghuhao1994@163.com> - 8.4.2-2
- fix build fail

* Wed Dec 11 2019 catastrowings <jianghuhao1994@163.com> - 8.2.1-2
- openEuler init

